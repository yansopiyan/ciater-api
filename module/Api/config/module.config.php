<?php
return [
    'service_manager' => [
        'factories' => [
            \Api\V1\Rest\Users\UsersResource::class => \Api\V1\Rest\Users\UsersResourceFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'api.rest.users' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/api/users[/:users_id]',
                    'defaults' => [
                        'controller' => 'Api\\V1\\Rest\\Users\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'api.rest.users',
        ],
    ],
    'zf-rest' => [
        'Api\\V1\\Rest\\Users\\Controller' => [
            'listener' => \Api\V1\Rest\Users\UsersResource::class,
            'route_name' => 'api.rest.users',
            'route_identifier_name' => 'users_id',
            'collection_name' => 'users',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [
                0 => 'sort',
                1 => 'username',
                2 => 'active',
            ],
            'page_size' => 25,
            'page_size_param' => 'pageSize',
            'entity_class' => \Api\V1\Rest\Users\UsersEntity::class,
            'collection_class' => \Api\V1\Rest\Users\UsersCollection::class,
            'service_name' => 'users',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'Api\\V1\\Rest\\Users\\Controller' => 'HalJson',
        ],
        'accept_whitelist' => [
            'Api\\V1\\Rest\\Users\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'Api\\V1\\Rest\\Users\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'zf-hal' => [
        'metadata_map' => [
            \Api\V1\Rest\Users\UsersEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.users',
                'route_identifier_name' => 'users_id',
                'hydrator' => \Zend\Hydrator\ObjectProperty::class,
            ],
            \Api\V1\Rest\Users\UsersCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.users',
                'route_identifier_name' => 'users_id',
                'is_collection' => true,
            ],
        ],
    ],
    'zf-content-validation' => [
        'Api\\V1\\Rest\\Users\\Controller' => [
            'input_filter' => 'Api\\V1\\Rest\\Users\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'Api\\V1\\Rest\\Users\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'username',
            ],
            1 => [
                'required' => false,
                'validators' => [],
                'filters' => [],
                'name' => 'password',
            ],
            2 => [
                'required' => false,
                'validators' => [],
                'filters' => [],
                'name' => 'active',
            ],
        ],
    ],
];
